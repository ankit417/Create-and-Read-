<!DOCTYPE html>
<html>
<head>

	<title>Coding task</title>
</head>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
<body>
<?php
//mysqli_connect('localhost','root','','usertest');

$name = $gender = $phone = $email = $address = $nationality = $dob = $education = $prefered_cont = '';
if(isset($_POST['submit']))
{
if(empty($_POST['name']))
{
echo "Please enter your full Name";
exit();
}
if(empty($_POST['gender']))
{
echo "please specify your gender";
exit();	
}
if(empty($_POST['phone']))
{
	echo "Please enter your phone number";
	exit();
}
if(empty($_POST['email']))
{
	echo "please enter the email address";
	exit();
}
if(empty($_POST['address']))
{
	echo "Please enter your address";
	exit();
}
if(empty($_POST['nationality']))
{
	echo "please enter your nationality";
	exit();
}
if(empty($_POST['dateofbirth']))
{
	echo "please enter your date of birth";
	exit();
}
if(empty($_POST['education']))
{
	 echo "please enter your education background";
	 exit();
}
if(empty($_POST['contact']))
{
	echo "please enter your preferred contact";
	exit();
}
else{

			$name = check_entry($_POST['name']);
			$gender = check_entry($_POST['gender']);
			$phone = check_entry($_POST['phone']);
			$email = check_entry($_POST['email']);
			$address = check_entry($_POST['address']);
			$nationality = check_entry($_POST['nationality']);
			$dob = check_entry($_POST['dateofbirth']);
			$education = check_entry($_POST['education']);
			$prefered_cont = check_entry($_POST['contact']);





				$values = array
				(
				$name,$gender,$phone,$email,$address,$nationality,$dob,$education,$prefered_cont
				);

				if( !file_exists("userdetail.csv")){
					$file = fopen("userdetail.csv","a+");
					$header_row = array(
						"Name","Gender","Phone","Email","Address","Nationality","Date of Birth","Education","Preferred contact"
					);
					fputcsv($file, $header_row);
				}else{
					$file = fopen("userdetail.csv","a+");
				}
				fputcsv($file,  $values);


				fclose($file);


				echo "File saved successfully";
				}



}
			function check_entry($entry)
			{
			$entry = trim($entry);
			$entry = stripslashes($entry);
			$entry = htmlspecialchars($entry);
			return $entry;
			}

?>



<h1>Coding task for Intern/ Jr. Software Engineer</h1>
<form method = "post" action = "index.php"  >
	<div class="form-group">

					Name <input type="text" name="name" value="<?PHP if(isset($_POST['name'])) echo htmlspecialchars($_POST['name']); ?>" ><br/>
					Gender<select name="gender" >
						<option value="male">Male</option>
						<option value ="female">Female</option>
						<option value ="other"> Other</option>
					</select><br/>
					Phone<input type="tel" name="phone" value="<?PHP if(isset($_POST['phone'])) echo htmlspecialchars($_POST['phone']); ?>"><br/>
					Email<input type="email" name="email" value="<?PHP if(isset($_POST['email'])) echo htmlspecialchars($_POST['email']); ?>"><br/>
					Address<input type="text" name="address" value="<?PHP if(isset($_POST['address'])) echo htmlspecialchars($_POST['address']); ?>"><br/>
					Nationality<input type="text" name="nationality" value="<?PHP if(isset($_POST['nationality'])) echo htmlspecialchars($_POST['nationality']); ?>"><br/>
					Date of birth<input type="text" name="dateofbirth" value="<?PHP if(isset($_POST['dateofbirth'])) echo htmlspecialchars($_POST['dateofbirth']); ?>"><br/>
					Education background<input type="text" name="education" value="<?PHP if(isset($_POST['education'])) echo htmlspecialchars($_POST['education']); ?>"><br/>
					Preferred contact<select name="contact" >
						<option value ="Email"> Email </option>
						<option value="Phone">Phone</option>
					</select><br/>
					<input type="submit" name="submit"><br/>
	</div>
</form>

<a href="checkforms.php">Check the form </a>
</body>
</html>